# Push the project to Gitlab

## Setup a Gitlab repository

1. Create an account on Gitlab.com or login if you already have an account.

![Gitlab](./images/gitlab_login.png)

2. Ask your instructor to add you in the group *SophiaConf2019*

3. Create your project in this group

Name it with your firstname and make sure to make it *Public* in the *Visibility*.

![Gitlab](./images/gitlab_project.png)

4. Push your project

Follow the instructions given on the repo landing page "Push an existing folder" in order to push your local project in your new Gitlab repository.

5. Configure a Gitlab runner

As shared runners on gitlab can take a while before running your jobs, we  have setup a private runner on [DigitalOcean](https://digitalocean.com). This runner is available for all the repository within the *SophiaConf2019* group.

Within the *Settings > CI/CD* menu, expand the *Runner* part and do the following actions:
- disable the shared runner
- make sure you can access the "Available group Runners".

![Gitlab](./images/gitlab_group_runner.png)

[Let's now add a CI/CD pipeline](./04-CI)
