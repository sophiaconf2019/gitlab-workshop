# Create service account

cat <<EOF > rbac.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
EOF

echo "Creaing rbac rules:"
kubectl apply -f rbac.yaml
echo

# Get cluster name from config
CLUSTER_NAME=$(kubectl config view --minify -o jsonpath={.current-context})
echo "Cluster name: $CLUSTER_NAME"
echo

# Get API Server URL from config
API_URL=$(kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$(kubectl config current-context)'") | .cluster."server"')
echo "API URL: $API_URL"
echo

# Get secret used by the service account
SECRET=$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
TOKEN=$(kubectl -n kube-system get secret $SECRET -o jsonpath='{.data.token}' | base64 --decode)
echo "Service token:"
echo $TOKEN
echo

# Get cluster CA
kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$(kubectl config current-context)'") | .cluster."certificate-authority-data"' | base64 --decode > cluster_ca.pem
echo "Cluster CA:"
cat cluster_ca.pem
