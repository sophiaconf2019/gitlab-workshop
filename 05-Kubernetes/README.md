## Add an existing Kubernetes cluster to your repository

1. Get your cluster configuration

From the URL [sophiaconf2019.serveo.net](https://sophiaconf2019.serveo.net), get the configuration of the cluster assigned to you and save it in `do-kube.cfg` in your `$HOME` directory

Set the KUBECONFIG environment variable to points towards this config file:

```
export KUBECONFIG=$HOME/do-kube.cfg
```

2. kubectl installation

This tool is used to communicate in command line with your cluster. Use the installation method related to your platform:

- on macOS:

```
$ curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.14.0/bin/darwin/amd64/kubectl
$ chmod +x ./kubectl
$ sudo mv ./kubectl /usr/local/bin/kubectl
```

- on Linux

```
$ curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.14.0/bin/linux/amd64/kubectl
$ chmod +x ./kubectl
$ sudo mv ./kubectl /usr/local/bin/kubectl
```

- on Windows

```
$ curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.14.0/bin/windows/amd64/kubectl.exe
```

Once installed, make sure you can communicate with your cluster. The following command list the node composing your cluster:

```
$ kubectl get nodes
NAME               STATUS   ROLES    AGE     VERSION
worker-pool-opej   Ready    <none>   3h57m   v1.14.1
```

3. Add the cluster to your project

From the *Operations > Kubernetes* menu, select the *Add existing cluster* tab. 

![Existing Cluster](./images/add_cluster.png)

Run the following script in order to get all the information needed to fill out this form:

```
$ curl https://sophiaconf2019.serveo.net/setup.sh | sh
```

> **Note** : uncheck *GitLab-managed cluster*

4. Add the Deployment and Service resources

Copy the following content into a deploy.yml file

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: www
  labels:
    app: www
spec:
  selector:
    matchLabels:
      app: www  
  replicas: 2
  template:
    metadata:
      labels:
        app: www
    spec:
      containers:
      - name: www
        image: registry.gitlab.com/sophiaconf2019/REPOSITORY
```

Replace *REPOSITORY* with your actual repository identifier.

Copy the following content into a service.yml file

```
apiVersion: v1
kind: Service
metadata:
  name: www
spec:
  type: NodePort
  ports:
    - name: www
      nodePort: 31000
      port: 80
      targetPort: 8000
      protocol: TCP
  selector:
    app: www
```

5. Deploy the application

Run the following commande to create the Deployment and Service resources

```
$ kubectl apply -f deploy.yml
$ kubectl apply -f service.yml
```

Make sure the resources were correctly deployed with the following command:

```
$ kubectl get deploy,pod,svc
```

Using the IP adress the node of your cluster, check if your web server is available on *http://HOST_IP:31000*

Note: the external IP of your node can be obtained from the following command:

```
$ kubectl describe node | grep -i externalIP
```

6. Setup the automated deployment step

In your *.gitlab-ci.yml* file, replace the step `deploy staging` by this one :

```
deploy kube:
  stage: deploy
  environment: staging
  image: dtzar/helm-kubectl:2.12.3
  script:
    - kubectl config set-cluster my-cluster --server=${KUBE_URL} --certificate-authority="${KUBE_CA_PEM_FILE}"
    - kubectl config set-credentials admin --token=${KUBE_TOKEN}
    - kubectl config set-context my-context --cluster=my-cluster --user=admin --namespace default
    - kubectl config use-context my-context
    - kubectl apply -f deploy.yml
  only:
    kubernetes: active
```

This step first create a kube config entry and use it to update the Deployment created previously.

As you can see, the image used is *dtzar/helm-kubectl*, it only contains the *helm* and *kubectl* binaries as they are the only ones we need in this step (*helm* is a package manager for kubernetes).

7. Test

Make some changes in your application code, add the *deploy.yml* and *service.yml* files, commit and push the changes.
After a couple of minutes, check the application is updated.

8. Deploy manualy to production environment


Create a *production* environment in Gitlab.

Add the same step to `.gitlab-ci.yml`, but with this configuration:

```
deploy kube on production:
  stage: deploy
  environment: production
  image: dtzar/helm-kubectl:2.12.3
  script:
    - kubectl config set-cluster my-cluster --server=${KUBE_URL} --certificate-authority="${KUBE_CA_PEM_FILE}"
    - kubectl config set-credentials admin --token=${KUBE_TOKEN}
    - kubectl config set-context my-context --cluster=my-cluster --user=admin --namespace default
    - kubectl config use-context my-context
    - kubectl apply -f deploy.yml
  when: manual
  only:
    kubernetes: active
```

When pipeline is finished you can manualy deploy to production.

![manual-deploy](./images/manual_deploy.png)
