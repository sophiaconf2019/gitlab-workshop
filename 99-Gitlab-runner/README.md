# Gitlab Runner


For your information, the commands to start a runner, **you don't have to perform them**.

* Configure:

```
docker run --rm -t -i \
 -v $(pwd):/etc/gitlab-runner \
 gitlab/gitlab-runner register \
   --non-interactive \
   --executor "docker" \
   --docker-image docker:stable \
   --url "https://gitlab.com/" \
   --registration-token "9zz8Zeh8JQADsTkWJLys" \
   --description "SophiaConf 2019 Runner" \
   --tag-list "docker" \
   --run-untagged \
   --locked="false" \
   --docker-privileged
```

* Start it

```
  docker run -d --name gitlab-runner --restart always \
  -v $(pwd):/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```