# Setup a Gitlab-CI pipeline

## Create a pipeline that automatically build and package the project

Create a `.gitlab-ci.yml` file in the root directory your project and make sure it contains the following content:

```
stages:
  - package

push image docker:
  image: docker:stable
  stage: package
  services:
    - docker:dind
  script:
    - docker build -t $CI_REGISTRY_IMAGE .
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE
```

This step defines a CI stage named *package* and a set of instructions to perform the build and push of the image.

Commit and push, go to gitlab and check in the CI/CD page that pipeline is running.

Within the *Registry* menu you should see the first image of your application has been build and is now available in the registry.

## Add some integration tests

Under the *stages* key, add a new *integration* entry.

```
stages:
  - package
  - integration
```

At the end of the *.gitlab-ci.yaml* file, add the following *integration test* step.

```
integration test:
  image: docker:stable
  stage: integration
  services:
    - docker:dind
  script:
    - docker run -d --name myapp $CI_REGISTRY_IMAGE
    - sleep 10s
    - TEST_RESULT=$(docker run --link myapp byrnedo/alpine-curl -s http://myapp:8000)
    - echo $TEST_RESULT
    - $([ "$TEST_RESULT" == "Hello World!" ])
```

This step defines a test of the newly created image. It makes sure the web server return the "Hello World!" string.

Commit and push the update and check the pipeline is running.

You should notice the job fails. Check the job's logs to find the bug, fix it, commit an push.

## Automatic deploy to staging

From the *Operations > Environments* menu, Create an environment `staging` in gitlab.

![staging](./images/env_staging_1.png)

![staging](./images/env_staging_2.png)

Under the *staging* key, add a new *deploy* entry:

```
stages:
  - package
  - integration
  - deploy
```

At the end of the *.gitlab-ci.yaml* file, add the following *deploy* step:

```
deploy staging:
  stage: deploy
  script:
    - echo "Deploy to staging server"
  environment:
    name: staging
    url: https://staging.example.com
```

Commit and push the update and check the pipeline is running.

You can have a look to the environment page:

![env-deployed](./images/env_deployed.png)

Ok, nothing has been actualy deployed: we will deploy on Kubernetes on next chapter.

## Manual deploy to production

Create a `production` environment in gitlab.

![production](./images/env_production.png)

It will be used later.

[Let's now deploy on a Kubernetes cluster](./05-Kubernetes)
